
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});

	$(document).ready(function() {
		//$("#myModal").modal('show');
	});

	function _json(str) {
		var obj = JSON.parse(str);
		return obj;
	}
	function isJson(text) {
    if (typeof text=="object") return true;
    else return false;
	}
	function _post(path, prm, callback) {
		$('body').addClass('waiting');
		isajax = true; //index.blade.php
		$.ajax({
			url: host + '/' + path, //'mypage.html',
			type: "POST",
			data: prm,
			success: function(res) {
				if (callback!='') _parse(callback, res);
				else {
					addalert("child", "Success " + res); //alert('success');
					isajax = false;
				}
			},
			error: function(err) {
				$('body').removeClass('waiting');
				addalert("child", "Error " + err); //alert(err);
				isajax = false;
			}
		});
	}

	function _parse(id, res) {
		var sp = id.split("|");
		$('body').removeClass('waiting');
		switch(sp[0]) {
			case "addbook": 	//index-.blade.php
				resaddbook(sp[1], res); //alert(res);
			break;
			case "updatecars": //car.blade.php
				resupd(sp[1], res);
			break;
			case "addcar": //car.blade.php
				resins(sp[1], res);
			break;
			case "delcar": //car.blade.php
				resdel(sp[1], res);
			break;
			case "viewemp": //index-.blade.php
				resviewemp(sp[1], res);
			break;
			case "checkschedule": //index-.blade.php
				rescheckschedule(sp[1], res);
			break;
			case "searchschedule": //cars.blade.php
				ressearchschedule(sp[1], res);
			break;
			case "rescarscheduleday": //cars.blade.php
				rescarscheduleday(sp[1], res);
			break; 
			case "allcars": //index.blade.php
				resallcars(sp[1], res);
			break; 
			default:
				//alert(res);
				addalert("child", "Respon unregistered");
		}
	isajax = false;
	}
  function _reload() {
    setTimeout(function() {
      location.reload();
    }, 1000);/**/
  }